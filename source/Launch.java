//import java.io.*;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

/**
 * Cette classe assure le lancement de l'application, via la mthode main. Elle
 * ne contient que la mthode "public static void main(...)" Une JFrame est
 * construite, puis rendue visible.
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class Launch extends JApplet{
		
	private static final long serialVersionUID = 1L;

	
	/**
	 * 
	 */
	public void init() {
		int frameWidth = 1000;
		int frameHeight = 800;
		
		String StrAxiom      ;
		String StrAngle      ;
		String StrProfondeur ;
		String StrLineLength ;
		String StrNbrRules      ;
		String[] Rules       ;
		
		String tempString;
		
		int NbrRules;
		
		Container content;

		content = this.getContentPane();		

		MainPanel window1 = new MainPanel(null);

		StrAxiom      = getParameter("axiom");
		StrAngle      = getParameter("angle");
		StrProfondeur = getParameter("profondeur");
		StrLineLength = getParameter("linelength");
		StrNbrRules   = getParameter("numberofrules");  
		
		NbrRules = Integer.parseInt(StrNbrRules);
		Rules = new String[NbrRules];
		
		for(int i=0 ; i<NbrRules ; i++ ){
			tempString = getParameter("rule"+i);
			
			if(tempString != null){
				Rules[i] = tempString;				
			}			
		}	
		
		window1.init(StrAxiom, StrAngle, StrProfondeur,  
		StrLineLength, NbrRules, Rules);
		
		content.add(window1);
		
		System.out.println("Lancement de la mthode init() : applet") ;
		
		this.setSize(new Dimension(frameWidth, frameHeight));
		this.setVisible(true);
	}

	/**
	 * On construit une JFrame et on la rend visible. Notons que les arguments
	 * de la ligne de commande peuvent tre transmis au constructeur de la
	 * Fenetre (hrite de JFrame). Nanmoins cette option n'est plus utile, ni
	 * utilise.
	 * 
	 * @param args
	 * @since 0.9
	 */
	public static void main(String args[]) {
		
		Container FrameContent;
		
		JFrame Frame_window1 = new JFrame("LSystem project");
		JPanel window1 = new MainPanel(args);
			
		System.out.println("Lancement de la mthode main(String args) : application locale") ;
		
		// pour obtenir une destruction propre de la fentre
		Frame_window1.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		Frame_window1.setSize(new Dimension(1000, 700));
		
		FrameContent = Frame_window1.getContentPane();
		FrameContent.add(window1);
		
		Frame_window1.setVisible(true);
	}

}
