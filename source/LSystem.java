//import java.io.*;
import java.util.*;
import java.awt.Graphics;

/**
 * Cette classe permet de reprsenter le LSystem, elle rcupre le travail de la classe
 * Engine au travers de l'attribut FinalString.<p>
 * L'attribut StateStack permet de grer la pile d'tats, ncessaire pour reprsenter les
 * LSystems.
 * On stocke l'tat courant dans l'attribut StateCour de type State.
 * <p>
 * La mthode cl de cette classe est la mthode Paint, elle permet de lire la chaine 
 * FinalString est de traduire chaque symbole significatif en une action sur StateCour.
 * <p>
 * On notera que les actions de dessin n'ont pas lieu dans cette classe, elles sont 
 * prises en charge par la classe State. 
 * 
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class LSystem {

	private String FinalString;

	private State StateCour;

	private Stack StateStack;

	/**
	 * Constructeur par dfaut. 
	 * Construction d'un tat de base, et initialisation de la pile et de FinalString.
	 * 
	 */
	public LSystem() {
		FinalString = new String();

		StateCour = new State(new Point(300, 50), 0, 30, 10);

		StateStack = new Stack();
	}

	/**
	 * Constructeur qui prend en argument une FinalString. 
	 * Construction d'un tat de base, et initialisation de la pile.
	 * 
	 * @param FinalString
	 */
	public LSystem(String FinalString) {
		this.FinalString = FinalString;

		StateCour = new State(new Point(300, 50), 0, 30, 10);

		StateStack = new Stack();
	}

	/**
	 * Constructeur qui prend en argument un tat de base et initialisation de la pile.
	 * 
	 * @param StateCour
	 */
	public LSystem(State StateCour) {
		this.StateCour = new State(StateCour);

		StateStack = new Stack();
	}	

	/**
	 * Cette mthode permet de rcuprer le rsultat du travail de la classe Engine via la
	 * mthode BuildLSystem() de Engine.
	 * On rcupre la chane finale reprsentant le LSystem souhait. 
	 * 
	 * 
	 * @param FinalString
	 */
	public void SetFinalString(String FinalString) {
		this.FinalString = FinalString;
	}

	/**
	 * Pour rcuprer la reprsentation du LSystem sous forme de chane.	 * 
	 * 
	 * @return Renvoie FinalString, chane final du LSystem
	 */
	public String GetFinalString() {
		return FinalString;
	}

	/**
	 * Mise  jour de l'tat courant.
	 * 
	 * @param NewState, instance de la classe State 
	 */
	public void SetStateCour(State NewState) {
		StateCour = NewState;
	}

	/**
	 * Mise  jour de l'tat courant.
	 * 
	 * @param px           offsetX de l'tat de base
	 * @param py           offsetY de l'tat de base
	 * @param Angle        Donne l'angle pour une rotation lmentaire de la tortue
	 * @param Length       Donne la longueur d'un dplacement lmentaire de la tortue
	 * @param InitialAngle Angle initial de reprsentation de la scne
	 */
	public void SetStateCour(float px, float py, float Angle, float Length, float InitialAngle) {
		StateCour = new State(new Point(px, py), Angle, Length, InitialAngle);		
	}

	/**
	 * Cette mthode lance l'affichage. 
	 * Cette mthode permet de lire la chaine FinalString est de traduire chaque 
	 * symbole significatif en une action sur StateCour.
	 * <p>
	 * Les symboles significatifs sont les suivants :
	 * <ul>
	 * <li> T : Avance la tortue et dessine un trait -> appel de AvanceDraw(Screen)
	 * <li> t : Avance la tortue -> appel de AvancePoint()
	 * <li> + : Effectuer une rotation lmentaire dans le sens trigo -> appel de Rotation(1)
	 * <li> - : Effectuer une rotation lmentaire dans le sens des aiguilles d'une montre
	 *  -> appel de Rotation(-1)
	 * <li> [ : Empiler l'tat courant -> appel de StateStack.push(State_temp)
	 * <li> ] : Rcuprer l'tat du sommet de la pile -> appel de StateStack.pop()
	 * </ul>
	 * 
	 * @param Screen Le contexte graphique que l'on transmet  la mthode AvanceDraw(Screen) 
	 * de la classe State
	 */
	public void Paint(Graphics Screen) {
		
		char CurChar;
		State State_temp = new State(StateCour);

		for (int i = 0; i < FinalString.length(); i++) {
			CurChar = FinalString.charAt(i);

			switch (CurChar) {
			case 'T':
				State_temp.AvanceDraw(Screen);
				break;

			case 't':
				State_temp.AvancePoint();
				break;

			case '+':
				State_temp.Rotation(1);
				break;

			case '-':
				State_temp.Rotation(-1);
				break;

			case '[':
				StateStack.push(new State(State_temp));
				break;

			case ']':
				State_temp = (State) StateStack.pop();
				break;
				
			case ';':
				State_temp.IncColor();
				break;
				
			case ':':
				State_temp.DecColor();
				break;				

			default:
				// System.out.println("Default case dans le switch");
				break;
			}
		}

	}

}
