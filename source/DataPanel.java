import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

//import java.io.*;

/**
 * Cette classe sert  rcuprer un texte, et  le convertir en chanes que l'on
 * envoie au tableau JTable de la classe RulesPanel. L'attribut TextCollector de
 * type JTextArea prsente deux avantages, il permet de saisir un texte ou bien
 * d'effectuer un copier/coller  partir d'un document externe.
 * <p>
 * Ce panneau a deux boutons :
 * <ul>
 * <li> Get Text : transfert du texte avec pr-traitement vers la JTable de
 * RulesPanel
 * <li> Clear collector : Flush du TextCollector de type JTextArea
 * </ul>
 * Lorsque l'utilisateur appuie sur le bouton "Get Text", le contenu de la zone
 * de texte est dcoup en autant de chanes de caractres qu'il contient de
 * lignes.
 * <p>
 * Ce sont ces chanes de caractres qui sont transmises via la mthode
 * ConvertToRules(),  la JTable de RulesPanel.
 * 
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class DataPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 00001000L;

	private JButton But_GetText;

	private JButton But_Clear;

	private JScrollPane scrollpane;

	private JTextArea TextCollector;

	/**
	 * Rfrence  une instance de RulesPanel, utile pour la mthode
	 * ConvertToRules()
	 */
	private RulesPanel RulesTabRef;

	/**
	 * Constructeur du DataPanel, initialisation et mise en forme des composants
	 * graphiques de ce panneau.
	 * 
	 */
	public DataPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), "Text collector"));

		Box box1 = Box.createHorizontalBox();

		TextCollector = new JTextArea(100, 200);
		// TextCollector.addActionListener(this);
		TextCollector.setMaximumSize(new Dimension(800, 500));

		scrollpane = new JScrollPane(TextCollector,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollpane.setMaximumSize(new Dimension(800, 500));
		box1.add(scrollpane);

		Box box2 = Box.createVerticalBox();

		But_GetText = new JButton("Get Text");
		But_GetText.addActionListener(this);
		But_Clear = new JButton("Clear collector");
		But_Clear.addActionListener(this);
		box2.add(But_GetText);
		box2.add(But_Clear);

		this.add(box1);
		this.add(box2);
	}

	/**
	 * Permet d'accder  la JTable de Rules Panel via la mthode
	 * ConvertToRules.
	 * 
	 * @param Rfrence
	 *             une instance de RulesPanel
	 */
	public void SetRulesTabRef(RulesPanel RulesTabRef) {
		this.RulesTabRef = RulesTabRef;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ev) {

		Object sourcev = ev.getSource();

		if (sourcev == But_GetText) {
			System.out.println("Action sur But_GetText !");

			ConvertToRules();
		}

		if (sourcev == But_Clear) {
			System.out.println("Action sur But_Clear !");

			try {
				TextCollector.replaceRange("", TextCollector
						.getLineStartOffset(0), TextCollector
						.getLineEndOffset(TextCollector.getLineCount() - 1));
			} catch (javax.swing.text.BadLocationException e) {
				System.out
						.println("Erreur au moment de la rcupration du texte, dans"
								+ "la mthode actionPerformed(ActionEvent ev) de la classe DataPanel");
			}
		}
	}

	/**
	 * Cette mthode permet de mettre  jour la JTable de RulesPanel, pour cela
	 * on rcupre les lignes contenues dans le TextCollector.
	 * <p>
	 * Notons que s'il y a plus de ligne dans le TextCollector que dans la
	 * JTable de RulesPanel, des lignes vides sont ajoutes  la JTable de
	 * RulesPanel en nombre suffisant.
	 * 
	 */
	public void ConvertToRules() {
		JTable tableau = RulesTabRef.GetTableau();
		DefaultTableModel dftable = RulesTabRef.Getdftable();

		int Nline = TextCollector.getLineCount();
		System.out.println("nombre de ligne : " + Nline);

		// to store the data from text collector
		String[] Rules = new String[Nline];

		int cur_line = 0;
		int curline_offset = 0;
		int nbrLineDif;

		if (tableau.getRowCount() < Nline) {
			nbrLineDif = Nline - tableau.getRowCount();

			for (int i = 0; i < nbrLineDif; i++) {
				dftable.addRow(new Object[1]);
			}
		}

		try {

			while (cur_line < Nline) {
				curline_offset = TextCollector.getLineStartOffset(cur_line);
				// System.out.println("line startoffset : " + curline_offset);

				int offset_dif = TextCollector.getLineEndOffset(cur_line)
						- curline_offset;

				String lignelue = TextCollector.getText(curline_offset,
						offset_dif);

				if (lignelue == null) {
					System.out.println("Ligne vide dans ConvertToRules() ");
					Rules[cur_line] = null;
					break;
				} else {
					tableau.setValueAt((Object) lignelue, cur_line, 0);
				}

				cur_line++;
			}
		} // fin bloc try

		catch (Exception e) {
			System.out.println("Grave erreur dans la mthode ConvertToRules() "
					+ "de la classe DataPanel !");
		}

	}
	
}
