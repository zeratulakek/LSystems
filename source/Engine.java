//import java.io.*;

/**
 * Cette classe est fondamentale pour la construction d'un LSystem puisqu'elle
 * gre la grammaire et l'alphabet, elle permet une construction symbolique du
 * LSystem.
 * <p>
 * Cette classe contient trois mthodes essentielles, que nous dcrirons ici,
 * succintement.
 * <ul>
 * <li> ExtractGenerators : Pour extraire les symboles auxquels une rgle est
 * associe
 * <li> Derivate : Pour driver la chaine courante et obtenir le LSystem de
 * profondeur +1
 * <li> BuildLSystem : Drive la chaine courante un nombre de fois gal 
 * Profondeur, en partant de l'axiome
 * </ul>
 * 
 * Concernant les mthodes Set(...), on considre qu'elles mettent  jour
 * correctement les champs important de la classe Engine. Cette faon de
 * procder est contestable, et un blindage additionnel ne serait peut tre pas
 * de trop.
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class Engine extends Thread {

	/**
	 * La chaine donnant la configuration initiale.
	 */
	private String Axiom;

	/**
	 * Les rgles du LSystem.
	 */
	private String[] Rules;

	/**
	 * Les symboles gnrateurs du LSystem.
	 */
	private char[] Generators;

	/**
	 * La chane qui contient l'tat courant de drivation.
	 */
	private String CurrentString;

	/**
	 * La profondeur de drivation.
	 */
	private int Profondeur;

	private boolean Alive;

	/**
	 * Le constructeur de la classe Engine. Notons qu'il n'effectue aucune
	 * action particulire.
	 * <p>
	 * On suppose lorsqu'on utilise une instance de la classe Engine que les
	 * champs de cette classe sont mis  jour correctemet via les mthodes
	 * Set(...) de la classe Engine.
	 * <p>
	 * Cette faon de procder est contestable, et un blindage additionnel ne
	 * serait peut tre pas de trop.
	 * 
	 */
	public Engine() {
		CurrentString = null;
		Alive = false;

	}

	/**
	 * Cette mthode permet de mettre  jour la chane courante. En fait, elle
	 * n'est utilise que dans la mthode actionPerformed(ActionEvent) de la
	 * classe Display. Au moment o on initialise le LSystem, en chargant dans
	 * la chane courante l'axiome initial.
	 * 
	 * @param Axiom
	 *            Le nom de ce paramtre reflte le fait que cette mthode ne
	 *            doit tre appele que pour initialiser CurrentString
	 */
	public void SetCurrentString(String Axiom) {
		this.CurrentString = Axiom;
	}

	/**
	 * Rcupration de la chane courante reprsentant le LSystem.
	 * 
	 * @return Rfrence  CurrentString
	 */
	public String GetCurrentString() {
		return CurrentString;
	}

	/**
	 * En fait, elle n'est utilise que dans la mthode
	 * actionPerformed(ActionEvent) de la classe Display. Au moment o l'on
	 * vient de rcuprer l'ensemble des rgles, via la JTable de RulesPanel.
	 * 
	 * @param Rules
	 */
	public void SetRules(String[] Rules) {
		this.Rules = Rules;
	}

	/**
	 * @param Profondeur
	 */
	public void SetProfondeur(int Profondeur) {
		this.Profondeur = Profondeur;
	}

	/**
	 * @return
	 */
	public int GetProfondeur() {
		return Profondeur;
	}

	/**
	 * Cette mthode n'est plus utilise, originellement elle tait appele par
	 * la mthode paint de la classe Display. Elle permet de lire l'axiome via
	 * un nom de fichier pass en premier argument de la ligne de commande.
	 * <p>
	 * Cette opration est dsormais effectue via un champ de texte dans la
	 * classe AxiomAndSettings.
	 * 
	 * 
	 * @param NomFich
	 *            Fichier qui contient l'axiome du LSystem
	 * @deprecated L'usage de cette mthode pour construire un LSystem est
	 *             dconseill
	 */
	public void ReadAxiom(String NomFich) {

		if (NomFich != null) {
			Axiom = Manip.ReadFile(NomFich)[0];
		} else {
			System.out.println("Aucun fichier n'a ete transmis en argument !");
		}

		System.out.println("Axiome initiale : ");
		System.out.println(Axiom);

		CurrentString = new String(Axiom);
	}

	/**
	 * Cette mthode n'est plus utilise, originellement elle tait appele par
	 * la mthode paint de la classe Display. Elle permet de lire l'ensemble des
	 * rgles via un nom de fichier pass en deuxime argument de la ligne de
	 * commande.
	 * <p>
	 * Cette opration est dsormais effectue via une JTable dans la classe
	 * RulesPanel.
	 * 
	 * @param NomFich
	 *            Fichier qui contient les rgles du LSystem
	 * @deprecated L'usage de cette mthode pour construire un LSystem est
	 *             dconseill
	 */
	public void ReadRules(String NomFich) {

		if (NomFich != null) {
			Rules = Manip.ReadFile(NomFich);
		} else {
			System.out.println("Aucun fichier n'a ete transmis en argument !");
		}

		System.out.println("Rgles lues : ");
		for (int i = 0; i < Rules.length; i++) {
			System.out.println(Rules[i]);
		}

	}

	/**
	 * Cette mthode permet de rcuprer les symboles associs  des rgles. En
	 * fait, on rcupre le caractre  gauche du gal dans les rgles.
	 * 
	 * 
	 */
	public void ExtractGenerators() {
		int NbrOfGen = 0;
		int RulesLength = Rules.length;

		int indice[] = new int[RulesLength];
		int RulesIndex[] = new int[RulesLength];

		String tempRules[];

		// on recherche les symboles gnrateurs parmi toutes les rgles
		for (int i = 0; i < RulesLength; i++) {
			indice[i] = Rules[i].indexOf('=');

			if (indice[i] == 1) {
				RulesIndex[NbrOfGen] = i;
				NbrOfGen++;
			}
		}
		for (int i = NbrOfGen; i < RulesLength; i++) {
			RulesIndex[i] = -1;
		}

		// System.out.println( "nombre de = trouvs : " + NbrOfGen ) ;

		Generators = new char[NbrOfGen];

		int i;
		for (i = 0; i < RulesLength; i++) {
			if (RulesIndex[i] != -1) {
				Generators[i] = Rules[RulesIndex[i]].charAt(0);
			}
		}

		// reconstruction de Rules[]
		int k = 0;
		tempRules = new String[NbrOfGen];
		for (i = 0; i < RulesLength; i++) {
			if (RulesIndex[i] != -1) {
				tempRules[k] = Rules[RulesIndex[i]];
				k++;
			}
		}

		Rules = new String[NbrOfGen];
		for (i = 0; i < NbrOfGen; i++) {
			Rules[i] = tempRules[i];
		}

		System.out.println("Les gnrateurs sont les suivants : ");
		for (i = 0; i < Generators.length; i++) {
			System.out.println(Generators[i]);
		}
		System.out.println("------------------------");

	}

	/**
	 * Cette mthode permet de driver la chane courante. On scanne la chane
	 * courante en utilisant le systme de rgles, et on effectue les
	 * remplacements ncessaires.
	 * 
	 */
	public void Derivate() {

		int i = 0;
		char CurChar;
		boolean added = false;

		String NextString = new String();

		for (i = 0; i < CurrentString.length(); i++) {
			CurChar = CurrentString.charAt(i);

			for (int j = 0; j < Generators.length; j++) {
				if (CurChar == Generators[j]) {
					// System.out.println( "Le symbole " + Generators[j] + "
					// va
					// tre driv." ) ;
					NextString += Rules[j].substring(2);
					added = true;
				}
			}

			if (!added) {
				NextString += CurChar;
				added = true;
			}

			added = false;			
		}

		CurrentString = new String(NextString);
		System.out.println(CurrentString);
	}

	/**
	 * Cette mthode drive la chane courante un nombre de fois gal 
	 * Profondeur.
	 * 
	 */
	public void BuildLSystem() {

		yield();

		for (int i = 0; i < Profondeur; i++) {
			Derivate();
		}

	}

	public void run() {

		Alive = true;
		BuildLSystem();
		Alive = false;
	}

	public void setAlive(boolean OnOff) {
		Alive = OnOff;
	}

	public boolean getAlive() {
		return Alive;
	}

}
