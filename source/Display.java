import javax.swing.*;

import java.awt.*;

import java.awt.event.*;

//import java.io.*;

/**
 * Cette classe lance la construction du LSystem via l'implmentation de la
 * mthode actionPerformed(ActionEvent). L'attribut motor de type Engine permet
 * d'effectuer cette construction.
 * <p>
 * Cette mthode rcupre trois caractristiques essentielles du LSystem et les
 * transmet  motor :
 * <ul>
 * <li> l'axiome initial
 * <li> les rgles
 * <li> la profondeur
 * </ul>
 * On appel ensuite via l'attribut motor, les deux mthodes de construction du
 * LSystem :
 * <ul>
 * <li> Extraction des symboles qui sont associs  une rgle :
 * ExtractGenerators()
 * <li> Construction du LSystem par drivation : BuildLSystem()
 * </ul>
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class Display extends JPanel implements ActionListener {

	private static final long serialVersionUID = 00001000L;

	private Engine motor;

	private LSystem LSys1;

	private AxiomAndSettings SettingsPanel;

	/**
	 * 
	 */
	private RulesPanel RulesTable;

	private JButton but_Draw;
	private JButton but_Reload;

	private JButton but_Stop;
	
	private JButton but_Prev;
	private JButton but_Next;

	/**
	 * Cet attribut garantit qu' chaque fois que l'utilisateur appuie sur le
	 * bouton Draw l'ensemble des paramtres de l'interface graphique est
	 * recharg.
	 * 
	 */
	private boolean firstDisplay;

	/**
	 * Initialisation de l'attribut LSys1, instance de LSystem ; et de
	 * l'attribut motor, instance de Engine.
	 * 
	 * @param args
	 *            La rcupration des arguments de la ligne de commande n'est
	 *            plus utile
	 */
	public Display(String args[]) {
		firstDisplay = true;

		Box box1 = Box.createHorizontalBox();
		box1.setMaximumSize(new Dimension(300, 80));

		motor = new Engine();

		// motor.SetProfondeur( 4 ) ;
		// motor.ReadAxiom( args[0] ) ;
		// motor.ReadRules( args[1] ) ;
		// motor.ExtractGenerators() ;

		LSys1 = new LSystem();

		but_Draw = new JButton("DRAW");
		but_Stop = new JButton("STOP");
		but_Reload = new JButton("Reload");
		but_Prev = new JButton("Previous iteration");
		but_Next = new JButton("Next iteration");
		
		but_Draw.addActionListener(this);
		but_Stop.addActionListener(this);
		but_Prev.addActionListener(this);
		but_Next.addActionListener(this);
		but_Reload.addActionListener(this);

		box1.add(but_Prev);
		box1.add(but_Draw);
		box1.add(but_Next);
		box1.add(but_Reload);
		
		box1.add(Box.createHorizontalStrut(30));
		box1.add(but_Stop);

		this.add(box1, "North");
	}

	/**
	 * 
	 * 
	 * @param SettingsPanel
	 */
	public void SetSettingsPanel(AxiomAndSettings SettingsPanel) {
		this.SettingsPanel = SettingsPanel;
	}

	/**
	 * 
	 * 
	 * @param RulesTable
	 */
	public void SetRulesTable(RulesPanel RulesTable) {
		this.RulesTable = RulesTable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ev) {

		Object sourcev = ev.getSource();		

		float AngleRead;
		float LengthRead;
		float InitialAngle;
		float px, py;
		
		int tempProf = SettingsPanel.GetProfondeur();

		if (sourcev == but_Draw || sourcev == but_Prev || sourcev == but_Next
				|| sourcev == but_Reload) {

			System.out.println("Appel de la procdure de dessin");
			firstDisplay = true;

			if (firstDisplay && sourcev != but_Reload) {
				// lecture de l'axiome
				motor.SetCurrentString(SettingsPanel.GetAxiom());
				// lecture de la rgle
				// motor.SetRule0( SettingsPanel.GetRule() ) ;
				motor.SetRules(RulesTable.GetRules());
				
				if(sourcev == but_Prev){
					if(tempProf>0){
						SettingsPanel.SetProfondeur(String.valueOf(tempProf-1));	
					}					
				}
				else if(sourcev == but_Next) {
					SettingsPanel.SetProfondeur(String.valueOf(tempProf+1));
				}				
				
				// lecture de la profondeur				
				motor.SetProfondeur(SettingsPanel.GetProfondeur());

				// lecture de l'angle et de la longueur de ttrait
				AngleRead = SettingsPanel.GetAngle();
				LengthRead = SettingsPanel.GetLength();
				InitialAngle = SettingsPanel.GetInitialAngle();

				// lecture du point o l'on place le lsystem
				px = SettingsPanel.GetPosX();
				py = SettingsPanel.GetPosY();

				// mise  jour StateCour
				LSys1.SetStateCour(px, py, AngleRead, LengthRead, InitialAngle);

				motor.ExtractGenerators();
				
//				motor.setAlive(true);				
//				motor.setPriority(Thread.MIN_PRIORITY);
//				motor.start();								
//				/*motor.run();*/
				
				motor.BuildLSystem();

				LSys1.SetFinalString(new String(motor.GetCurrentString()));
				firstDisplay = false;
			}
			else if(firstDisplay){				
				// lecture de l'angle et de la longueur de ttrait
				AngleRead = SettingsPanel.GetAngle();
				LengthRead = SettingsPanel.GetLength();
				// lecture de l'angle de rotation de la scne
				InitialAngle = SettingsPanel.GetInitialAngle();
				
				// lecture du point o l'on place le lsystem
				px = SettingsPanel.GetPosX();
				py = SettingsPanel.GetPosY();
				
				// mise  jour StateCour
				LSys1.SetStateCour(px, py, AngleRead, LengthRead, InitialAngle);
			}			
			
			this.repaint();
		}

		if(sourcev == but_Stop) {			
			motor.setAlive(false);
			try {
				motor.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			motor = new Engine();
			
//			System.out.println("Thread.MAX_PRIORITY : "+Thread.MAX_PRIORITY);
//			System.out.println("Thread.MIN_PRIORITY : "+Thread.MIN_PRIORITY);
//			System.out.println("Priorit courante : "+motor.getPriority());
		}		
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		System.out.println("Appel de painComponent ");

		LSys1.Paint(g);
	}

}
