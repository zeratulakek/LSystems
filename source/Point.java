//import java.io.*;

public class Point {

	private float x;

	private float y;

	public Point() {
		x = 10;
		y = 10;
		// System.out.println("initialisation par dfaut");
	}

	public Point(Point pt) {
		x = pt.x;
		y = pt.y;
		// System.out.println("initialisation par point");
	}

	public Point(float x, float y) {
		this.x = x;
		this.y = y;
		// System.out.println("initialisation basique");
	}

	public void set_x(float x) {
		this.x = x;
	}

	public void set_y(float y) {
		this.y = y;
	}

	public float get_x() {
		return (x);
	}

	public float get_y() {
		return (y);
	}

	public String toString() {
		String chaine = null;

		chaine = new String("Coordonnes de ce point.\n");
		chaine += new String("x = " + x + " y = " + y + "\n");

		return (chaine);
	}

}
