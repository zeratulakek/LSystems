import javax.swing.*;
import java.awt.*;

//import java.awt.event.MouseEvent;
//import java.awt.event.MouseListener;

//import java.io.*;

/**
 * Cette classe est le conteneur principal de l'interface graphique. Elle hrite
 * de JFrame, il ne faut donc pas oublier de rcuprer via getContentPane()
 * l'objet associ de type Container auquel nous ajouterons les autres objets
 * graphiques de l'interface. Tout ce qui apparat  l'cran a t ajout
 * directement ou indirectement  ce conteneur.
 * <p>
 * Cette classe contient une structure en onglets,  deux onglets.
 * <p>
 * Le premier onglet contient :
 * <ul>
 * <li> une instance d'AxiomAndSettings drive de JPanel
 * <li> une instance de RulesPanel drive de JPanel
 * <li> une instnace de DisplayPanel drive de JPanel
 * </ul>
 * <p>
 * Le deuxime onglet contient :
 * <ul>
 * <li> une instance de DataPanel
 * </ul>
 * 
 * @author Mathieu Drouin
 * @version 0.9
 */
public class MainPanel extends JPanel {

	private static final long serialVersionUID = 00001000L;

	/**
	 * Permet la mise en place de la structure en onglets de l'interface
	 * graphique.
	 */
	private JTabbedPane TabbedPane;

	/**
	 * Ce panneau contient les principaux paramtres et l'axiome.
	 */
	private AxiomAndSettings SettingsPanel;

	/**
	 * Ce panneau contient les rgles.
	 */
	private RulesPanel RulesTable;

	/**
	 * Ce panneau permet l'affichage du LSystem.
	 */
	private Display DisplayPanel;

	/**
	 * JPanel intermdiaire en rapport avec la gestion du Layout.
	 */
	private JPanel LSystems_panel;

	/**
	 * Ce panneau permet de rcuprer des donnes sous forme de texte.
	 */
	private DataPanel Data_panel;

	/**
	 * Unique constructeur de la classe MainPanel. On y appelle les constructeurs
	 * des diffrents panneaux, et on gre la mise en forme globale de
	 * l'interface graphique.
	 * 
	 * @param args
	 */
	public MainPanel(String[] args) {

		// dimensions du domaine
		this.setMaximumSize(new Dimension(1000, 700));

		// this.setLayout( new FlowLayout() ) ;

		LSystems_panel = new JPanel();
		LSystems_panel.setMaximumSize(new Dimension(1000, 700));

		Data_panel = new DataPanel();
		Data_panel.setMaximumSize(new Dimension(1000, 700));

		TabbedPane = new JTabbedPane();
		TabbedPane.setPreferredSize(new Dimension(990, 690));

		SettingsPanel = new AxiomAndSettings();
		RulesTable = new RulesPanel();
		DisplayPanel = new Display(args);

		DisplayPanel.SetSettingsPanel(SettingsPanel);
		DisplayPanel.SetRulesTable(RulesTable);

		Data_panel.SetRulesTabRef(RulesTable);

		SettingsPanel.setBackground(Color.pink);
		RulesTable.setBackground(Color.cyan);
		DisplayPanel.setBackground(Color.white);

		LSystems_panel
				.setLayout(new BoxLayout(LSystems_panel, BoxLayout.X_AXIS));

		Box box11 = Box.createVerticalBox();
		// box11.setMaximumSize( new Dimension(200, 800) ) ;

		box11.add(SettingsPanel);
		box11.add(RulesTable);

		LSystems_panel.add(box11 /* , "West" */);
		LSystems_panel.add(DisplayPanel /* , "Center" */);

		TabbedPane.addTab("LSystems", LSystems_panel);
		TabbedPane.addTab("Datas", Data_panel);		
		
		this.add(TabbedPane);
	}

	
	public void init(String StrAxiom, String StrAngle, String StrProfondeur, 
			String StrLineLength, int NbrRules, String[] Rules) {		
		
		SettingsPanel.SetAxiom(StrAxiom);
		SettingsPanel.SetAngle(StrAngle);
		SettingsPanel.SetProfondeur(StrProfondeur);
		SettingsPanel.SetLength(StrLineLength);
		
		RulesTable.SetRules(Rules, NbrRules);		
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
		
//		Component ParentWindow;
//		ParentWindow=this.getParent();		
		
		Dimension tempDim;
		
		super.paintComponent(g);
		
		this.setMaximumSize(tempDim=new Dimension(this.getParent().getSize()));
		TabbedPane.setPreferredSize(tempDim);
		
//		new Dimension(TabbedPane.getParent().getSize())
		
		System.out.println(tempDim);		
		
//		Graphics GC = this.getGraphics();				
//		super.paintComponent(GC);
				
//		System.out.println(this.getParent());

		System.out.println("Re-taillage de la fentre");
	}
	

}

